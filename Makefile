
CMP = g++ -std=c++11
CLASS = Piece
CLASS2 = Board
MAIN = main
EXEC = Chess

$(EXEC): $(CLASS).o $(CLASS2).o $(MAIN).o
	$(CMP) -g $(FLAGS) $(CLASS).o $(CLASS2).o $(MAIN).o -o $(EXEC)

$(CLASS).o: $(CLASS).cpp $(CLASS).h
	$(CMP) -g -c $(CLASS).cpp -o $(CLASS).o

$(CLASS2).o: $(CLASS2).cpp $(CLASS2).h
	$(CMP) -g -c $(CLASS2).cpp -o $(CLASS2).o

$(MAIN).o: $(MAIN).cpp $(CLASS).h
	$(CMP) -g -c $(MAIN).cpp -o $(MAIN).o

clean:
	rm *.o
	rm $(EXEC)

