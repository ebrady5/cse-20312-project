// Piece.h
// Chris and Edmund Brady
// Interface for the Chess Pieces

#include <stdio.h>
using namespace std;

class Piece {
 public:
  Piece();
  ~Piece();
  int getPiece(); // we will use integers to store the pieces... 1 = pawn, 2 = knight, etc.
  void setPiece(int);
  int getStatus();
  void setStatus(int);
  int getColor();
  void setColor(int);
 private:
  int type; // 0 = empty, 1 = pawn, 2 = knight, 3 = rook, 4 = bishop, 5 = queen, 6 = king
  int live; // 1 = live, 0 = dead
  int color; // 0 = white, 1 = black. Color will have to match the player
};
