// board.h
// Edmund Brady
// Implementation for Chess board

#include "Piece.h"
#include <stdio.h>
using namespace std;

class Board {
 public:
  Board();
  ~Board();
  void printBoard();
  void setPiece(int, int, int, int, int); // row, column, type, live status, color
  int getType(int, int);
  int getStat(int, int);
  int getColor(int, int);
  void boardInit();

  // Pawn Pieces
  int pawnCheckForward(int, int, int, int); // row, column, row destination, column destination
  int pawnCheckDoubleForward(int, int, int, int); // this one will need to check if it's first turn
  int pawnCheckDiagLeft(int, int, int, int); // checks for piece to attack
  int pawnCheckDiagRight(int, int, int, int);
  int pawnCheckAll(int, int, int, int); // syntax of the "check all" -- do an || or for all of that piece's functions, return 1 if any of them returns 1 and return 0 otherwise... (row, col, rowDest[ination], colDest)

  // Knight Pieces
  int knightCheckUpLeftV(int, int, int, int);
  int knightCheckUpLeftH(int, int, int, int);
  int knightCheckUpRightV(int, int, int, int);
  int knightCheckUpRightH(int, int, int, int);
  int knightCheckDownRightV(int, int, int, int);
  int knightCheckDownRightH(int, int, int, int);
  int knightCheckDownLeftV(int, int, int, int);
  int knightCheckDownLeftH(int, int, int, int);
  int knightCheckAll(int, int, int, int);

  // Rook Pieces
  int rookCheckUp(int, int, int, int); 
  int rookCheckDown(int, int, int, int);
  int rookCheckLeft(int, int, int, int);
  int rookCheckRight(int, int, int, int);
  int rookCheckAll(int, int, int, int);

  // Bishop Pieces
  int bishopCheckUpLeft(int, int, int, int);
  int bishopCheckUpRight(int, int, int, int);
  int bishopCheckDownLeft(int, int, int, int);
  int bishopCheckDownRight(int, int, int, int);
  int bishopCheckAll(int, int, int, int);

  // Queen Pieces
  int queenCheckUp(int, int, int, int);
  int queenCheckUpRight(int, int, int, int);
  int queenCheckRight(int, int, int, int);
  int queenCheckDownRight(int, int, int, int);
  int queenCheckDown(int, int, int, int);
  int queenCheckDownLeft(int, int, int, int);
  int queenCheckLeft(int, int, int, int);
  int queenCheckUpLeft(int, int, int, int);
  int queenCheckAll(int, int, int, int);

  // King Pieces
  int kingCheckUp(int, int, int, int);
  int kingCheckUpRight(int, int, int, int);
  int kingCheckRight(int, int, int, int);
  int kingCheckDownRight(int, int, int, int);
  int kingCheckDown(int, int, int, int);
  int kingCheckDownLeft(int, int, int, int);
  int kingCheckLeft(int, int, int, int);
  int kingCheckUpLeft(int, int, int, int);
  int kingCheckAll(int, int, int, int);

  // Game stats -- to check if game is over
  int checkForWhiteKing();
  int checkForBlackKing();

 private:
  Piece gameBoard[8][8]; // 8x8 2D Matrix of Pieces
};

