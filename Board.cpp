// Board.cpp
// Edmund Brady
// Implementation for the Chess Board

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
using namespace std;

#include "Board.h"

Board::Board() {
  boardInit();
}

Board::~Board()
{
  // no dynamic allocation...
}

void Board::printBoard()
{
  char color;
  //  cout << endl << "     Chess     " << endl << endl << "   0 1 2 3 4 5 6 7" << endl << "   ---------------" << endl;
  cout << endl << "          - Chess -         " << endl << endl << "    0  1  2  3  4  5  6  7" << endl << "   -------------------------" << endl;
  int colorNum, printNum;
  for (int i = 0; i < 8; i++) {
    cout << i << " | ";
    for (int j = 0; j < 8; j++) {
      printNum = getType(i, j);
      colorNum = getColor(i, j);
      if (colorNum == 0) {
	color = 'W';
      }
      else if (colorNum == 1) {
	color = 'B';
      }
      if (getType(i, j) == 0) {
	color = ' ';
      }
      if (getType(i, j) > 0) {
	cout << printNum << color << " ";
      }
      else {
	cout << "   ";
      }
    }
    if (i == 7) {
      cout << "|" << endl;
    }
    else {
      cout << "|" << endl << "  |-------------------------|" << endl;
    }
  }
  cout << "   -------------------------" << endl << endl;
}

void Board::setPiece(int row, int col, int pType, int pLive, int pColor)
{
  gameBoard[row][col].setPiece(pType);
  gameBoard[row][col].setStatus(pLive);
  gameBoard[row][col].setColor(pColor);
}

int Board::getType(int row, int col)
{
  int returnVal;
  returnVal = gameBoard[row][col].getPiece();
  return returnVal;
}

int Board::getStat(int row, int col)
{
  int returnVal;
  returnVal = gameBoard[row][col].getStatus();
  return returnVal;
}

int Board::getColor(int row, int col)
{
  int returnVal;
  returnVal = gameBoard[row][col].getColor();
  return returnVal;
}

void Board::boardInit()
{
  // black pieces
  setPiece(0, 0, 3, 1, 1);
  setPiece(0, 1, 2, 1, 1);
  setPiece(0, 2, 4, 1, 1);
  setPiece(0, 3, 5, 1, 1);
  setPiece(0, 4, 6, 1, 1);
  setPiece(0, 5, 4, 1, 1);
  setPiece(0, 6, 2, 1, 1);
  setPiece(0, 7, 3, 1, 1);
  // pawns
  for (int i = 0; i < 8; i++) {
    setPiece(1, i, 1, 1, 1);
  }

  // white pieces
  // pawns
  for (int i = 0; i < 8; i++) {
    setPiece(6, i, 1, 1, 0);
  }
  setPiece(7, 0, 3, 1, 0);
  setPiece(7, 1, 2, 1, 0);
  setPiece(7, 2, 4, 1, 0);
  setPiece(7, 3, 5, 1, 0);
  setPiece(7, 4, 6, 1, 0);
  setPiece(7, 5, 4, 1, 0);
  setPiece(7, 6, 2, 1, 0);
  setPiece(7, 7, 3, 1, 0);

  // middle of the board
  for (int i = 2; i < 6; i++) {
    for (int j = 0; j < 8; j++ ) {
      setPiece(i, j, 0, 0, 0);
    }
  }
  
}

// cout testing version

/*
int Board::pawnCheckForward(int row, int col)
{
  if (getType(row, col) != 1) {
    cout << "not a pawn.\n";
    return 0;
  }

  if (getStat(row, col) == 1) {
    cout << "getStat = 1\n";
    cout << "color = " << getColor(row, col) << endl;
    if (getColor(row, col) == 0) { // white piece
      cout << "getStat(row-1, col) = " << getStat(row-1, col) << endl;
      if (getStat(row-1, col) == 0) { // check one space up
	cout << "check space is empty\n";
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      cout << "getStat(row+1, col) = " << getStat(row+1, col) << endl;
      if (getStat(row+1, col) == 0) { // check one space down
	cout << "check space is empty\n";
	return 1;
      }
    }
  }
  else {
    return 0;
  }
}
*/

// -----------------------------------------------------------------------------------------------
 // Pawn
// -----------------------------------------------------------------------------------------------

int Board::pawnCheckForward(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 1) { // not a pawn
    return 0;
  }
  if (colDest != col) { // make sure we're moving vertically
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if (rowDest != row-1) {
	return 0;
      }
      if (getStat(row-1, col) == 0) { // check one space up
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (rowDest != row+1) {
	return 0;
      }
      if (getStat(row+1, col) == 0) { // check one space down
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::pawnCheckDoubleForward(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 1) { // not a pawn
    return 0;
  }
  if (colDest != col) { // make sure we're moving vertically
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if (row != 6) { // not first turn
	return 0;
      }
      if (rowDest != row-2) {
	return 0;
      }
      if (getStat(row-2, col) == 0) { // check two spaces up
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (row != 1) { // not first turn
	return 0;
      }
      if (rowDest != row+2) {
	return 0;
      }
      if (getStat(row+2, col) == 0) { // check two spaces down
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::pawnCheckDiagLeft(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 1) { // not a pawn
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if (rowDest != row-1 || colDest != col-1) { // make sure we're moving to right spot
	return 0;
      }
      if (getStat(rowDest, colDest == 0)) {
	return 0;
      }
      if (getStat(row-1, col-1) == 1 && getColor(row-1, col-1) != 0) { // check for a piece to attack
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (rowDest != row+1 || colDest != col-1) {
	return 0;
      }
      if (getStat(rowDest, colDest == 0)) {
	return 0;
      }
      if (getStat(row+1, col-1) == 1 &&getColor(row+1, col-1) != 1) { // check for a piece to attack
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::pawnCheckDiagRight(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 1) { // not a pawn
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if (rowDest != row-1 || colDest != col+1) { // make sure we're moving to right spot
	return 0;
      }
      if (getStat(rowDest, colDest == 0)) {
	return 0;
      }
      if (getStat(row-1, col+1) == 1 && getColor(row-1, col+1) !=0) { // check for a piece to attack
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (rowDest != row-1 || colDest != col+1) { // make sure we're moving to right spot
	return 0;
      }
      if (getStat(rowDest, colDest == 0)) {
	return 0;
      }
      if (getStat(row+1, col+1) == 1 && getColor(row+1, col+1) != 0) { // check for a piece to attack
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::pawnCheckAll(int row, int col, int rowDest, int colDest) {
  if (pawnCheckForward(row, col, rowDest, colDest) == 1 || pawnCheckDoubleForward(row, col, rowDest, colDest) == 1 || pawnCheckDiagLeft(row, col, rowDest, colDest) == 1 || pawnCheckDiagRight(row, col, rowDest, colDest) == 1) {
    return 1;
  }
  else {
    return 0;
  }
}

// -----------------------------------------------------------------------------------------------
// Knight
// -----------------------------------------------------------------------------------------------

int Board::knightCheckUpLeftV(int row, int col, int rowDest, int colDest) // more vertical ==> 2 up, one left
{
  if (getType(row, col) != 2) { // not a knight
    return 0;
  }

  if (colDest != col-1 || rowDest != row -2) { // make sure we're moving to the right place
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if ((getStat(row-2, col-1) == 0) || (getStat(row-2, col-1) == 1 && getColor(row-2, col-1) == 1)) { // check for an empty space or a piece to attack
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if ((getStat(row-2, col-1) == 0) || (getStat(row-2, col-1) == 1 && getColor(row-2, col-1) == 0)) {
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::knightCheckUpLeftH(int row, int col, int rowDest, int colDest) // more horizontal ==> 1 up, 2 left
{
  if (getType(row, col) != 2) { // not a knight
    return 0;
  }

  if (colDest != col-2 || rowDest != row-1) { // make sure we're moving to the right place
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if ((getStat(row-1, col-2) == 0) || (getStat(row-1, col-2) == 1 && getColor(row-1, col-2) == 1)) { // check for an empty space or a piece to attack
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if ((getStat(row-1, col-2) == 0) || (getStat(row-1, col-2) == 1 && getColor(row-1, col-2) == 0)) {
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::knightCheckUpRightV(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 2) { // not a knight
    return 0;
  }

  if (colDest != col+1 || rowDest != row -2) { // make sure we're moving to the right place
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if ((getStat(row-2, col+1) == 0) || (getStat(row-2, col+1) == 1 && getColor(row-2, col+1) == 1)) { // check for an empty space or a piece to attack
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if ((getStat(row-2, col+1) == 0) || (getStat(row-2, col+1) == 1 && getColor(row-2, col+1) == 0)) {
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::knightCheckUpRightH(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 2) { // not a knight
    return 0;
  }

  if (colDest != col+2 || rowDest != row-1) { // make sure we're moving to the right place
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if ((getStat(row-1, col+2) == 0) || (getStat(row-1, col+2) == 1 && getColor(row-1, col+2) == 1)) { // check for an empty space or a piece to attack
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if ((getStat(row-1, col+2) == 0) || (getStat(row-1, col+2) == 1 && getColor(row-1, col+2) == 0)) {
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}


int Board::knightCheckDownRightV(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 2) { // not a knight
    // cout << "not a knight\n";
    return 0;
  }

  if (colDest != col+1 || rowDest != row+2) { // make sure we're moving to the right place
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      //cout << "dest stat = " << getStat(row+2, col+1) << endl;
      //cout << "dest color = " << getColor(row+2, col+1) << endl;

      if ((getStat(row+2, col+1) == 0) || (getStat(row+2, col+1) == 1 && getColor(row+2, col+1) == 1)) { // check for an empty space or a piece to attack
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if ((getStat(row+2, col+1) == 0) || (getStat(row+2, col+1) == 1 && getColor(row+2, col+1) == 0)) {
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::knightCheckDownRightH(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 2) { // not a knight
    return 0;
  }

  if (colDest != col+2 || rowDest != row+1) { // make sure we're moving to the right place
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if ((getStat(row+1, col+2) == 0) || (getStat(row+1, col+2) == 1 && getColor(row+1, col+2) == 1)) { // check for an empty space or a piece to attack
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if ((getStat(row+1, col+2) == 0) || (getStat(row+1, col+2) == 1 && getColor(row+1, col+2) == 0)) {
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::knightCheckDownLeftV(int row, int col, int rowDest, int colDest) // more vertical ==> 2 down, one left
{
  if (getType(row, col) != 2) { // not a knight
    return 0;
  }

  if (colDest != col-1 || rowDest != row+2) { // make sure we're moving to the right place
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if ((getStat(row+2, col-1) == 0) || (getStat(row+2, col-1) == 1 && getColor(row+2, col-1) == 1)) { // check for an empty space or a piece to attack
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if ((getStat(row+2, col-1) == 0) || (getStat(row+2, col-1) == 1 && getColor(row+2, col-1) == 0)) {
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::knightCheckDownLeftH(int row, int col, int rowDest, int colDest) // more horizontal ==> 1 down, 2 left
{
  if (getType(row, col) != 2) { // not a knight
    return 0;
  }

  if (colDest != col-2 || rowDest != row+1) { // make sure we're moving to the right place
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    if (getColor(row, col) == 0) { // white piece
      if ((getStat(row+1, col-2) == 0) || (getStat(row+1, col-2) == 1 && getColor(row+1, col-2) == 1)) { // check for an empty space or a piece to attack
	return 1;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if ((getStat(row+1, col-2) == 0) || (getStat(row+1, col-2) == 1 && getColor(row+1, col-2) == 0)) {
	return 1;
      }
    }
  }

  else {
    return 0;
  }
}

int Board::knightCheckAll(int row, int col, int rowDest, int colDest) {
  if (knightCheckUpLeftV(row, col, rowDest, colDest) ==1 || knightCheckUpLeftV(row, col, rowDest, colDest) == 1 || knightCheckUpRightV(row, col, rowDest, colDest) == 1 || knightCheckUpRightH(row, col, rowDest, colDest) == 1 || knightCheckDownRightV(row, col, rowDest, colDest) == 1 || knightCheckDownRightH(row, col, rowDest, colDest) == 1 || knightCheckDownLeftV(row, col, rowDest, colDest) == 1 || knightCheckDownLeftH(row, col, rowDest, colDest) == 1) {
    return 1;
  }
  else {
    return 0;
  }
}


// -----------------------------------------------------------------------------------------------
// Rook
// -----------------------------------------------------------------------------------------------

int Board::rookCheckUp(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 3) { // not a rook
    return 0;
  }

  if (colDest != col) {
    return 0;
  }

  if (rowDest >= row) { // make sure we go in the right direction
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = row-1; i > rowDest; i--) {
	// cout << "i = " << i << endl;
	// cout << "hit a piece bbycakes\n\n\n";
	if (getStat(i, col) == 1) { // make sure the column is empty
	  //cout << "hit a piece\n";
	  trueFlag = 0;
	  return 0;
	}
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = row-1; i > rowDest; i--) {
	cout << "hit a piece bbycakes\n\n\n";
	if (getStat(i, col) == 1) { // make sure the column is empty
	  trueFlag = 0;
	  return 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::rookCheckDown(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 3) { // not a rook
    return 0;
  }

  if (colDest != col) {
    return 0;
  }

  if (rowDest <= row) {
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col) == 1) { // make sure the column is empty
	  trueFlag = 0;
	  return 0;
	}
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col) == 1) { // make sure the column is empty
	  trueFlag = 0;
	  return 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::rookCheckLeft(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 3) { // not a rook
    return 0;
  }

  if (rowDest != row) {
    return 0;
  }

  if (colDest >= col) {
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = col-1; i > colDest; i--) {
	if (getStat(row, i) == 1) { // make sure the column is empty
	  trueFlag = 0;
	  return 0;
	}
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = col-1; i > colDest; i--) {
	if (getStat(row, i) == 1) { // make sure the column is empty
	  trueFlag = 0;
	  return 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::rookCheckRight(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 3) { // not a rook
    return 0;
  }

  if (rowDest != row) {
    return 0;
  }

  if (colDest <= col) {
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = col+1; i < colDest; i++) {
	if (getStat(row, i) == 1) { // make sure the column is empty
	  trueFlag = 0;
	  return 0;
	}
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = col+1; i < colDest; i++) {
	if (getStat(row, i) == 1) { // make sure the column is empty
	  trueFlag = 0;
	  return 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::rookCheckAll(int row, int col, int rowDest, int colDest) {
  if (rookCheckUp(row, col, rowDest, colDest) == 1 || rookCheckDown(row, col, rowDest, colDest) == 1 || rookCheckLeft(row, col, rowDest, colDest) == 1 || rookCheckRight(row, col, rowDest, colDest) == 1) {
    return 1;
  }
  else {
    return 0;
  }
}

// -----------------------------------------------------------------------------------------------
// Bishop
// -----------------------------------------------------------------------------------------------

int Board::bishopCheckUpLeft(int row, int col, int rowDest, int colDest) {
  if (getType(row, col) != 4) { // not a bishop
    return 0;
  }

  if (abs(rowDest - row) != abs(colDest - col) || row <= rowDest || col <= colDest) { // make sure we're moving properly/diagonally
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row-1; i > rowDest; i--) {
	if (getStat(i, col-j) == 1) { // make sure the column is empty
	  //cout << "hit a piece\n";
	  trueFlag = 0;
	}
	j++;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row-1; i > rowDest; i--) {
	if (getStat(i, col-j) == 1) { // make sure the column is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::bishopCheckUpRight(int row, int col, int rowDest, int colDest) {
  if (getType(row, col) != 4) { // not a bishop
    return 0;
  }

  if (abs(rowDest - row) != abs(colDest - col) || row <= rowDest || col >= colDest) { // make sure we're moving properly/diagonally
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row-1; i > rowDest; i--) {
	if (getStat(i, col+j) == 1) { // make sure the diagonal is empty
	  //cout << "hit a piece\n";
	  trueFlag = 0;
	}
	j++;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row-1; i > rowDest; i--) {
	if (getStat(i, col+j) == 1) { // make sure the diagonal is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::bishopCheckDownRight(int row, int col, int rowDest, int colDest) {
  if (getType(row, col) != 4) { // not a bishop
    return 0;
  }

  int trueFlag = 0;

  if (abs(rowDest - row) != abs(colDest - col) || row >= rowDest || col >= colDest) { // make sure we're moving properly/diagonally
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col+j) == 1) { // make sure the diagonal is empty
	  //cout << "hit a piece\n";
	  trueFlag = 0;
	}
	j++;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col+j) == 1) { // make sure the diagonal is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::bishopCheckDownLeft(int row, int col, int rowDest, int colDest) {
  if (getType(row, col) != 4) { // not a bishop
    return 0;
  }

  if (abs(rowDest - row) != abs(colDest - col) || row >= rowDest || col <= colDest) { // make sure we're moving properly/diagonally
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col-j) == 1) { // make sure the diagonal is empty
	  //cout << "hit a piece\n";
	  trueFlag = 0;
	}
	j++;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col-j) == 1) { // make sure the diagonal is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::bishopCheckAll(int row, int col, int rowDest, int colDest) {
  if (bishopCheckUpLeft(row, col, rowDest, colDest) == 1 || bishopCheckUpRight(row, col, rowDest, colDest) == 1 || bishopCheckDownLeft(row, col, rowDest, colDest) == 1 || bishopCheckDownRight(row, col, rowDest, colDest) == 1) {
    return 1;
  }
  else {
    return 0;
  }
}

// -----------------------------------------------------------------------------------------------
// Queen
// -----------------------------------------------------------------------------------------------

int Board::queenCheckUp(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 5) { // not a queen
    return 0;
  }

  if (colDest != col) {
    return 0;
  }

  if (rowDest >= row) { // make sure we go in the right direction
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = row-1; i > rowDest; i--) {
	//cout << "i = " << i << endl;
	if (getStat(i, col) == 1) { // make sure the column is empty
	  //cout << "hit a piece\n";
	  trueFlag = 0;
	  return 0;
	}
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = row-1; i > rowDest; i--) {
	if (getStat(i, col) == 1) { // make sure the column is empty
	  trueFlag = 0;
	  return 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::queenCheckUpRight(int row, int col, int rowDest, int colDest) {
  if (getType(row, col) != 5) { // not a queen
    return 0;
  }

  int trueFlag = 0;

  if (abs(rowDest - row) != abs(colDest - col) || row <= rowDest || col >= colDest) { // make sure we're moving properly/diagonally
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row-1; i > rowDest; i--) {
	if (getStat(i, col+j) == 1) { // make sure the diagonal is empty
	  //cout << "hit a piece\n";
	  trueFlag = 0;
	}
	j++;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row-1; i > rowDest; i--) {
	if (getStat(i, col+j) == 1) { // make sure the diagonal is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::queenCheckRight(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 5) { // not a queen
    return 0;
  }

  if (rowDest != row) {
    return 0;
  }

  if (colDest <= col) { // make sure we go in the right direction
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = col+1; i < colDest; i++) {
	if (getStat(row, i) == 1) { // make sure the row is empty
	  trueFlag = 0;
	}
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = col+1; i < colDest; i++) {
	if (getStat(row, i) == 1) { // make sure the column is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::queenCheckDownRight(int row, int col, int rowDest, int colDest) {
  if (getType(row, col) != 5) { // not a queen
    return 0;
  }

  int trueFlag = 0;

  if (abs(rowDest - row) != abs(colDest - col) || row >= rowDest || col >= colDest) { // make sure we're moving properly/diagonally
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col+j) == 1) { // make sure the diagonal is empty
	  //cout << "hit a piece\n";
	  trueFlag = 0;
	}
	j++;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col+j) == 1) { // make sure the diagonal is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::queenCheckDown(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 5) { // not a queen
    return 0;
  }

  if (colDest != col) {
    return 0;
  }

  if (rowDest <= row) { // make sure we go in the right direction
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col) == 1) { // make sure the column is empty
	  trueFlag = 0;
	}
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col) == 1) { // make sure the column is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::queenCheckDownLeft(int row, int col, int rowDest, int colDest) {
  if (getType(row, col) != 5) { // not a queen
    return 0;
  }

  int trueFlag = 0;

  if (abs(rowDest - row) != abs(colDest - col) || row >= rowDest || col <= colDest) { // make sure we're moving properly/diagonally
    return 0;
  }

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col-j) == 1) { // make sure the diagonal is empty
	  //cout << "hit a piece\n";
	  trueFlag = 0;
	}
	j++;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row+1; i < rowDest; i++) {
	if (getStat(i, col-j) == 1) { // make sure the diagonal is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::queenCheckLeft(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 5) { // not a queen
    return 0;
  }

  if (rowDest != row) {
    return 0;
  }

  if (colDest >= col) { // make sure we go in the right direction
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = col-1; i > colDest; i--) {
	if (getStat(row, i) == 1) { // make sure the column is empty
	  trueFlag = 0;
	}
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      for (int i = col-1; i > colDest; i--) {
	if (getStat(row, i) == 1) { // make sure the column is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::queenCheckUpLeft(int row, int col, int rowDest, int colDest) {
  if (getType(row, col) != 5) { // not a queen
    return 0;
  }

  if (abs(rowDest - row) != abs(colDest - col) || row <= rowDest || col <= colDest) { // make sure we're moving properly/diagonally
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row-1; i > rowDest; i--) {
	if (getStat(i, col-j) == 1) { // make sure the column is empty
	  //cout << "hit a piece\n";
	  trueFlag = 0;
	}
	j++;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      int j = 1;
      for (int i = row-1; i > rowDest; i--) {
	if (getStat(i, col-j) == 1) { // make sure the column is empty
	  trueFlag = 0;
	}
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::queenCheckAll(int row, int col, int rowDest, int colDest) {
  if (queenCheckUp(row, col, rowDest, colDest) == 1 || queenCheckDown(row, col, rowDest, colDest) == 1 || queenCheckLeft(row, col, rowDest, colDest) == 1 || queenCheckRight(row, col, rowDest, colDest) == 1 || queenCheckUpLeft(row, col, rowDest, colDest) == 1 || queenCheckUpRight(row, col, rowDest, colDest) == 1 || queenCheckDownLeft(row, col, rowDest, colDest) == 1 || queenCheckDownRight(row, col, rowDest, colDest) == 1) {
    return 1;
  }
  else {
    return 0;
  }
}

// -----------------------------------------------------------------------------------------------
// King
// -----------------------------------------------------------------------------------------------

int Board::kingCheckUp(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 6) { // not a King
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row-1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row-1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::kingCheckUpRight(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 6) { // not a King
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row-1 || colDest != col+1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row-1 || colDest != col+1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::kingCheckRight(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 6) { // not a queen
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (colDest != col+1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (colDest != col+1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::kingCheckDownRight(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 6) { // not a King
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row+1 || colDest != col+1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row+1 || colDest != col+1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::kingCheckDown(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 6) { // not a King
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row+1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row+1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::kingCheckDownLeft(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 6) { // not a King
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row+1 || colDest != col-1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row+1 || colDest != col-1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::kingCheckLeft(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 6) { // not a queen
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (colDest != col-1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (colDest != col-1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::kingCheckUpLeft(int row, int col, int rowDest, int colDest)
{
  if (getType(row, col) != 6) { // not a King
    return 0;
  }

  int trueFlag = 0;

  if (getStat(row, col) == 1) { // piece is active
    trueFlag = 1;
    if (getColor(row, col) == 0) { // white piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 0) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row-1 || colDest != col-1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
    else if (getColor(row, col) == 1) { // black piece
      if (getStat(rowDest, colDest) == 1 && getColor(rowDest, colDest) == 1) { // can't attack piece of same color
	trueFlag = 0;
	return 0;
      }
      if (rowDest != row-1 || colDest != col-1) { // make sure we're moving to right spot
	trueFlag = 0;
	return 0;
      }
    }
  }

  if (trueFlag == 1) {
    return 1;
  }

  else {
    return 0;
  }
}

int Board::kingCheckAll(int row, int col, int rowDest, int colDest) {
  if (kingCheckUp(row, col, rowDest, colDest) == 1 || kingCheckDown(row, col, rowDest, colDest) == 1 || kingCheckLeft(row, col, rowDest, colDest) == 1 || kingCheckRight(row, col, rowDest, colDest) == 1 || kingCheckUpLeft(row, col, rowDest, colDest) == 1 || kingCheckUpRight(row, col, rowDest, colDest) == 1 || kingCheckDownLeft(row, col, rowDest, colDest) == 1 || kingCheckDownRight(row, col, rowDest, colDest) == 1) {
    return 1;
  }
  else {
    return 0;
  }
}

// -----------------------------------------------------------------------------------------------
// Game stats, etc.
// -----------------------------------------------------------------------------------------------

int Board::checkForWhiteKing() {
  int kingIsHere = 0;
  for (int i = 0; i <= 7; i++) {
    for (int j = 0; j <= 7; j++) {
      if (getStat(i, j) == 1 && getColor(i, j) == 0 && getType(i, j) == 6) {
	kingIsHere = 1;
      }
    }
  }
  // cout << "White King Status: " << kingIsHere << endl; // for debugging
  if (kingIsHere == 1) {
    return 1;
  }
  else {
    return 0;
  }
}

int Board::checkForBlackKing() {
  int kingIsHere = 0;
  for (int i = 0; i <= 7; i++) {
    for (int j = 0; j <= 7; j++) {
      if (getStat(i, j) == 1 && getColor(i, j) == 1 && getType(i, j) == 6) {
	kingIsHere = 1;
      }
    }
  }
  // cout << "Black King Status: " << kingIsHere << endl; // for debugging
  if (kingIsHere == 1) {
    return 1;
  }
  else {
    return 0;
  }
}
