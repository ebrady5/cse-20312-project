C++ OOP Chess
-------------
Chris Brady, Eddie Brady
(cbrady7, ebrady5)

This is a C++ implementation of Chess using Object-Oriented programming syntax. In its current state, the game is entirely text-based, although we originally planned to use the SDL2 graphics library for the game representation. When the respective user is prompted to make a move, the syntax is to enter [row column row_destination column_destination], with each separated by a press of the spacebar -- this will take the piece at a given row and column and move it to a desired row and column. If the desired move is invalid, the user will be notified and be prompted to try again. The exectuable "Chess" can be created by entering "make" at the command line. By following the given prompts, two users can play a functional game of Chess. Thank you for checking out our project!