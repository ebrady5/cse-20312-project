// Piece.cpp
// Chris and Edmund Brady
// Implementation for the Piece class

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include "Piece.h"

Piece::Piece()
{
  type = 0;
  live = 0;
  color = 0;
}

Piece::~Piece()
{
  // Nothing is dynamically allocated per piece
}

int Piece::getPiece()
{
  return type;
}

void Piece::setPiece(int pieceType)
{
  type = pieceType;
}

int Piece::getStatus()
{
  return live;
}

void Piece::setStatus(int pieceStat)
{
  live = pieceStat;
}

int Piece::getColor()
{
  return color;
}

void Piece::setColor(int pieceColor)
{
  color = pieceColor;
}

