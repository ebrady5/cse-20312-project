// main.cpp
// Edmund and Chris Brady
// Main Chess Driver

#include <stdio.h>
#include <iomanip>
#include <iostream>
#include <fstream>
#include "Board.h"
#include <stdlib.h>
#include <string>

int playerSwitch(int current) {
  if (current == 0) {
    return 1;
  }
  else if (current == 1) {
    return 0;
  }
}

int main(int argc, char *argv[])
{
  Board a;
  //  a.printBoard();

  int gameOver = 0;
  int currentPlayer = 0;
  int row, col, rowDest, colDest, validMove, moveMade;

  while(gameOver == 0) {
    system("clear");
    moveMade = 0;
    validMove = 1;
    a.printBoard();
    while(moveMade == 0) {
      // Check if game is still going
      if (a.checkForWhiteKing() == 0 || a.checkForBlackKing() == 0) {
	gameOver = 1;
	if (currentPlayer == 0) {
	  cout << "Game Over -- Player 2 (Black Team) wins!\n";
	}
	else if (currentPlayer == 1) {
	  cout << "Game Over -- Player 1 (White Team) wins!\n";
	}
	break;
      }
      
      validMove = 1; // reset if user made invalid move before

      if (currentPlayer == 0) {
	cout << "White, please make a move (row source, column cource, row destination, column destination): ";
	cin >> row >> col >> rowDest >> colDest;
	if (a.getColor(row, col) != 0) {
	  cout << "Please pick one of your own pieces.\n";
	  validMove = 0;
	}
      }
      else if (currentPlayer == 1) {
	cout << "Black, please make a move (row source, column source, row destination, column destination): ";
	cin >> row >> col >> rowDest >> colDest;
	if (a.getColor(row, col) != 1 && a.getStat(row, col) == 1) {
	  cout << "Please pick one of your own pieces.\n";
	  validMove = 0;
	}
      }
    
      if (validMove == 1) {
	if (a.pawnCheckAll(row, col, rowDest, colDest) == 1) {
	  a.setPiece(rowDest, colDest, 1, 1, currentPlayer);
	  a.setPiece(row, col, 0, 0, 0);
	  moveMade = 1;
	}
	else if (a.knightCheckAll(row, col, rowDest, colDest) == 1) {
	  a.setPiece(rowDest, colDest, 2, 1, currentPlayer);
	  a.setPiece(row, col, 0, 0, 0);
	  moveMade = 1;
	}
	else if (a.rookCheckAll(row, col, rowDest, colDest) == 1) {
	  a.setPiece(rowDest, colDest, 3, 1, currentPlayer);
	  a.setPiece(row, col, 0, 0, 0);
	  moveMade = 1;      
	}
	else if (a.bishopCheckAll(row, col, rowDest, colDest) == 1) {
	  a.setPiece(rowDest, colDest, 4, 1, currentPlayer);
	  a.setPiece(row, col, 0, 0, 0);
	  moveMade = 1;
	}
	else if (a.queenCheckAll(row, col, rowDest, colDest) == 1) {
	  a.setPiece(rowDest, colDest, 5, 1, currentPlayer);
	  a.setPiece(row, col, 0, 0, 0);
	  moveMade = 1;
	}
	else if (a.kingCheckAll(row, col, rowDest, colDest) == 1) {
	  a.setPiece(rowDest, colDest, 6, 1, currentPlayer);
	  a.setPiece(row, col, 0, 0, 0);
	  moveMade = 1;
	}
      }
      
      if (moveMade == 0) {
	cout << "Invalid move.\n";
      }

      //
    }
    currentPlayer = playerSwitch(currentPlayer);
  }

  /*

  cout << "Pawn move forward checks:\n";
  cout << "hi, checking 1,2: ";
  // cout << "getStat = " << a.getStat(1,2) << endl;
  cout << a.pawnCheckAll(1,2,1,3) << endl;
  cout << "hi, checking 5,5: ";
  cout << a.pawnCheckAll(5,5,0,0) << endl;
  cout << "hi, checking 0,0: ";
  cout << a.pawnCheckAll(0,0,1,0) << endl;
  cout << "hi, checking 6,5: ";
  cout << a.pawnCheckAll(6,5,6,4) << endl;

  cout << "Knight checks:\n";
  cout << "hi, checking 0,1:\n";
  // cout << "getStat = " << a.getStat(1,2) << endl;
  cout << "Horizonal DR: " << a.knightCheckAll(0,1,2,2) << endl;
  cout << "Vertical DR: " << a.knightCheckAll(0,1,1,3) << endl;

  cout << "Rook checks:\n";
  cout << "hi, checking 7,0:\n";
  cout << "Up: " << a.rookCheckUp(7,0, 3,0) << endl;
  cout << "hi, checking 0,7:\n";
  cout << "Left: " << a.rookCheckRight(0,7, 0,1) << endl;
  cout << "Right: " << a.rookCheckRight(0,7, 0,7) << endl;

  */

  return 0;
  }
